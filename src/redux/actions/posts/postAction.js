import {
    GET_POSTS,
    POST_POSTS,
    PUT_POSTS,
    DEL_POSTS
} from './postActionTypes'
import axios from 'axios'
export const getPosts=()=>{
    const innerGetPosts= async (dispatch)=>{
        const result=await axios.get("http://110.74.194.125:3535/api/category")
        dispatch({
            type: GET_POSTS,
            data:result.data
        })
    }
    return innerGetPosts
}
export const postPosts=(category)=>{
    console.log(category)
    const innerPostPosts= async (dispatch)=>{
        const result=await axios.post("http://110.74.194.125:3535/api/category",category)
        dispatch({
            type: POST_POSTS,
            data:result.data
        })
    }
    return innerPostPosts
}
// export const editPosts=(id,category)=>{
//     console.log(id)
//     const innerEditPosts= async (dispatch)=>{
//         const result=await axios.put(`http://110.74.194.125:3535/api/${category}/`+id)
//         dispatch({
//             type: PUT_POSTS,
//             data:result
//         })
//     }
//     return innerEditPosts
// }
export const deletePosts=(id)=>{
    console.log(id)
    const innerDeletePosts= async (dispatch)=>{
        const result=await axios.delete(`http://110.74.194.125:3535/api/category/`+id)
        dispatch({
            type: DEL_POSTS,
            data:result
        })
    }
    return innerDeletePosts
}

