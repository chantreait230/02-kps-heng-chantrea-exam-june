import { GET_POSTS, POST_POSTS, PUT_POSTS, DEL_POSTS } from "../../actions/posts/postActionTypes"

const noState={
    data:[],
    category:{},
    edit:{},
    delete:{}
}

export const postReducer=(state=noState,action) =>{
    switch(action.type){
        case POST_POSTS:
            return{
                ...state,
                category:action.data
            }
        case GET_POSTS:
            return {
                ...state, 
                data:action.data.data
            }
        case PUT_POSTS:
            return{
                ...state,
                edit:action.data.data
            }
        case DEL_POSTS:
                return{
                    ...state,
                    delete:action.data.data
                }    
        default:
            return state
    }
}