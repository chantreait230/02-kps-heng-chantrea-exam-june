import { combineReducers } from "redux";
import { postReducer } from "./postReducers/postReducer";

const reducers={
    postReducer: postReducer
}
export const rootReducer = combineReducers(reducers);