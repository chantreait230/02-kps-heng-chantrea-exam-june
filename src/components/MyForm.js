// import React, { Component } from 'react'
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import { Form,Col,Button,Table } from 'react-bootstrap';
// import{
//   getPosts,
//   postPosts,
//   editPosts,
//   deletePosts
// } from '../redux/actions/posts/postAction';
// class MyForm extends Component {

//     constructor(props) {
//         super(props)
    
//         this.state = {
//              data:{
//                  name:''
//              }
//         }
//     }
//     componentWillMount(){
//         this.props.getPosts();
//     }
//     componentDidUpdate(){
//         this.props.getPosts();
//     }
//     handleChange=(e)=>{
    
//         this.setState({data:{
//             ...this.state.data,
//             name:e.target.value,
//         }})
//         console.log(this.state.data.category)
//     }
//     handleEdit=(data)=>{
//         this.state.data.name=data.name
//     }
//     render() {
//         // 
//         var listData=this.props.data.map((data,index)=>
//             <tr key={data._id}>
//             <td>{index+1}</td>
//             <td>{data.name}</td>
//             <td>
//                 <Button variant="primary mx-2"
//                     onClick={()=>this.handleEdit(data)}
//                 >Edit</Button>
//                 <Button variant="danger"
//                     onClick={()=>this.props.deletePosts(data._id)}
//                 >Delete</Button>
//             </td>
//             </tr>
//         )
//         return (
//             <div>
//                 <div className="container" style={{marginTop:"50px"}}>
//                     <Form>
//                         <Form.Row>
//                         <Col>
//                             <Form.Control 
//                                 name="category" 
//                                 value={this.state.data.name}
//                                 onChange={(e)=>this.handleChange(e)}
//                                 placeholder="Category" 
//                              />
//                         </Col>
//                         <Col>
//                             <Button variant="danger"
//                                 onClick={()=>this.props.postPosts(this.state.data)}
//                             >Add Category</Button>
//                         </Col>
//                         </Form.Row>
//                     </Form>
//                     <Table striped bordered hover size="sm" style={{marginTop:"20px"}}>
//                         <thead>
//                         <tr>
//                             <th>#</th>
//                             <th>Name</th>
//                             <th>Action</th>
//                         </tr>
//                         </thead>
//                         <tbody>
//                             {listData}
//                         </tbody>
//                     </Table>
                
//                 </div>
//             </div>
//         )
//     }
// }
// const mapStateToProps = state => {
//     return{
//       data: state.postReducer.data
//     }
//   }
  
//   const mapDispatchToProps = dispatch => {
//     return bindActionCreators({
//       getPosts,
//       postPosts,
//       editPosts,
//       deletePosts
//     },dispatch)
//   }
  
// export default connect(mapStateToProps,mapDispatchToProps)(MyForm);
